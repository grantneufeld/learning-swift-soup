# Learning SwiftSoup

This is a test project to figure out how to work with
[SwiftSoup](https://scinfu.github.io/SwiftSoup/),
a Swift language library for parsing data from web pages.

## Explanation

Currently, we have “extractors” which are used to download data from a URL
then pull data from it using SwiftSoup.

The two we’ve tried so far are for the Wikipedia and Bloomberg websites.

So far, we’ve just looked at processing raw html.
We haven’t yet done anything with doing JavaScript processing before doing the processing.

## Usage

So far, there’s no point to building and running the app.
All of the work is currently done just through unit tests.

So, in Xcode, open the project and look in the
`learning-swift-soupTests` directory to see the tests.

You can then run individual tests,
or issue the "Product" -> "Test" menu command to run all of the tests.

## Notes on how the tests are written

The term `sut`, used in unit tests, is the standard practice for naming the “subject under test”.
Using that consistently allows us to look at any test
and know what is the thing the test is focused on.

There is debate on how to name the test `func`s.
Tests must always start with `test` to be found and run by the test runner.
But after that, it’s up to the coder on how to name them.
I’ve taken to using underscores (“snake-case”) to make the test functions very distinct from
any other functions in the code. The first part after `test_` describes what is being tested,
and the part after that describes the expectation.

## Warnings

This is not good code for actual use … yet.

In particular, it’s _very_ bad form to be downloading data from remote websites in unit tests.

Hopefully this will all become more complete and correct
as we continue doing this learning exercise.

## Contributors

* [Grant Neufeld](https://gitlab.com/grantneufeld)
* [Femi Aliu](https://gitlab.com/graygoos)

## License

MIT License

Copyright (c) 2023 Grant Neufeld

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
