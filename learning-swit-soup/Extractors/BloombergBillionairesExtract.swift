import Foundation
import SwiftSoup

// This is an example of the HTML we’re looking at:

//<section class="container-width">
//    <div class="table-chart">
//        <div class="table-row">
//            <div class="table-cell t-rank">
//            1
//            </div>
//            <div class="table-cell t-name"><a href="./profiles/bernard-j-arnault/">
//            Bernard Arnault</a></div>
//            <div class="table-cell active t-nw">
//            $190B
//            </div>
//            <div class="table-cell t-lcd neg">
//            -$801M
//            </div>
//
//            <div class="table-cell t-ycd pos">
//            +$27.8B
//            </div>
//
//            <div class="table-cell t-country">
//            France
//            </div>
//            <div class="table-cell t-industry">
//            Consumer
//            </div>
//        </div>

/// Get an array of strings with data from the Bloomberg Billionaires web page.
///
/// - Usage: `arrayOfStrings = try BloombergBillionairesExtract.getAllTheData()`
class BloombergBillionairesExtract {
    private static let billionairesListAddress = "https://www.bloomberg.com/billionaires/"
    /// The data downloaded from the web page.
    private var data: Data?
    /// The web page data html as a string.
    private var html: String = ""
    /// The SwiftSoup `Document` used to parse the html.
    private var doc: Document = Document("")

    /// Get all the billionaire data from Bloomberg.
    ///
    /// Really just a wrapper around creating an instance of this class and calling its method for getting the data.
    ///
    /// - Returns: Array of Strings.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    static func getAllTheData() throws -> [String] {
        let extractor = BloombergBillionairesExtract()
        return try extractor.getAllTheData()
    }

    /// Get all the billionaire data from Bloomberg.
    /// - Returns: Array of Strings.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func getAllTheData() throws -> [String] {
        self.html = downloadData(from: Self.billionairesListAddress)
        self.doc = try SwiftSoup.parse(html)
        let section = try findBillionairesSection()
        let div = try findTableChartDiv(section: section!)
        let rows = try findTableRows(div: div!)
        let result = try dataFromRows(rows: rows)
        return result
    }

    /// Try to find the right "section" element that holds the list.
    /// - Returns: The section as a SwiftSoup `Element`.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func findBillionairesSection() throws -> Element? {
        // an html "section" element with the "class" attribute containing "container-width"
        let section = try doc.select("section[class$=container-width").first()
        return section
    }

    /// Look in the section to find the table chart element that holds the rows.
    /// - Parameter section: The section `Element` from `findBillionairesSection()`.
    /// - Returns: The table chart as a SwiftSoup `Element`.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func findTableChartDiv(section: Element) throws -> Element? {
        // an html "div" element with the "class" attribute containing "table-chart"
        let div = try section.select("div[class$=table-chart").first()
        return div
    }

    /// Look in the table chart div to find the rows.
    /// - Parameter div: The div `Element` from `findTableChartDiv(section:)`.
    /// - Returns: The rows as a SwiftSoup `Elements` collection.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func findTableRows(div: Element) throws -> Elements {
        // html "div" elements with the "class" attribute containing "table-row"
        let rows = try div.select("div[class$=table-row")
        return rows
    }

    /// Get the data for each row.
    /// - Parameter rows: The `Elements` collection of rows from `findTableRows(div:)`.
    /// - Returns: `Array<String>` with data from each row.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func dataFromRows(rows: Elements) throws -> [String] {
        var result: [String] = []

        for row in rows {
            let rowData = try dataFromRow(row: row)
            result.append(rowData)
        }

        return result
    }

    /// Get the data for an individual row.
    ///
    /// Currently, we’re extracting the name, net worth, most recent change in net worth, and year-to-date change in net worth.
    ///
    /// - Parameter row: An individual row `Element` from the collection passed in to `dataFromRows(rows:)`.
    /// - Returns: A string with the extracted data.
    /// - Throws: Any errors thrown by SwiftSoup while trying to parse the html.
    private func dataFromRow(row: Element) throws -> String {
        let rowDivs = try row.select("div")
        let nameDiv = try row.select("div[class$=t-name]")
        let name = try nameDiv.text(trimAndNormaliseWhitespace: true)
        let netWorthDiv = try row.select("div[class$=t-nw]")
        let netWorth = try netWorthDiv.text(trimAndNormaliseWhitespace: true)

        // There seems to be an error in SwiftSoup’s parsing
        // when a class is not the last class in the "class" attribue.
        // So, instead of the following, we’re resorting to finding the cell by index offset:
        // let lastChangeDiv = try row?.select("div[class$=t-lcd]")
        let lastChangeDiv = rowDivs[4]
        let lastChange = try lastChangeDiv.text(trimAndNormaliseWhitespace: true)

        // Same issue as for the last change:
        // let ytdChangeDiv = try row?.select("div[class$=t-ycd]")
        let ytdChangeDiv = rowDivs[5]
        let ytdChange = try ytdChangeDiv.text(trimAndNormaliseWhitespace: true)

        return "\(name ): NW: \(netWorth ), last change: \(lastChange ), ytd change: \(ytdChange )"
    }

    /// Download a web page to get it’s html.
    /// - Parameter from: The URL to download, as a `String`.
    /// - Returns: The html of the page, as a `String`.
    private func downloadData(from urlString: String) -> String {
        // turn the string into an URL object
        let url = URL(string: urlString)

        // we’ll just use the default URL session configuration for now (not doing anything special)
        let sessionConfig = URLSessionConfiguration.default
        // this is the session we’ll use for downloading the page
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        // we’re just going to make one request, since we just want one page
        let request = NSMutableURLRequest(url: url!)
        // "GET" is the basic http method for when you just want to download/get the document at an URL
        request.httpMethod = "GET"
        // we have to make a task because doing network actions (like downloading a file from an URL)
        // needs to be multi-threaded so the app doesn’t “freeze” while waiting for the network
        // stuff to finish
        let task = session.dataTask(
            with: request as URLRequest,
            completionHandler: { (data: Data!, response: URLResponse!, error: Error!) -> Void in
                if (error == nil) {
                    // Success

                    // http status codes are 3-digit numbers.
                    // in this case, for a simple download, it will normally be "200"
                    // (why "200"? because http status codes are weird 😆)
                    //let statusCode = (response as! HTTPURLResponse).statusCode
                    //print("Success: \(statusCode)")

                    // This is your file-variable:
                    self.data = data
                }
                else {
                    // Failure

                    // something went wrong with downloading (network issue? remote server error? …?)
                    // instead of using `print()` here,
                    // in a real app you’d report the error to the user
                    print("Failure: %@", error.localizedDescription);
                }
            }
        )
        // this is where we tell the download task to actually start.
        // even though we haven’t previously started it, the function `resume()` is used.
        // names in programming are weird 🤔
        task.resume()

        // we have to wait for the download to complete
        // we’ll know it’s complete when there is some data
        while data == nil {
            // sleep tells the current thread to “sleep”,
            // so any other threads will get time to run instead
            sleep(1)
        }
        // the above loop won’t let us get to this point if data is `nil`
        // so we can safely force the optional to resolve as a concrete instance
        return String(data: data!, encoding: .utf8)!
    }
}
