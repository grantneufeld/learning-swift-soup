import SwiftUI

@main
/// Just the default SwiftUI app wrapper, for now.
struct learning_swit_soupApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
