@testable import learning_swit_soup
import XCTest

final class WikipediaReaderTests: XCTestCase {
    /// This is just trying out the `WikipediaReader.example()` method,
    /// where we do a few extractions of text from some very simple html.
    func test_example_returns_text() {
        let sut = WikipediaReader()
        let result = sut.example()
        XCTAssertEqual(result, ["Dog","Bird","Cat"])
    }

    /// Are we actually downloading the html?
    func test_downloadData_returns_the_html() {
        let sut = WikipediaReader()
        let html = sut.downloadData(from: WikipediaReader.root)
        // Just check that we got some html,
        // since the specific html will change as the page is updated by Wikipedia.
        XCTAssert(html.hasPrefix("<!DOCTYPE html>"))
    }

    /// Are we getting the right link?
    func test_findRandomLink_returnsTheLink() throws {
        let sut = WikipediaReader()
        let result = try sut.findRandomLink()
        XCTAssertEqual(result, "/wiki/Special:Random")
    }
}
