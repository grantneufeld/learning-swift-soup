@testable import learning_swit_soup
import XCTest

final class BloombergBillionairesExtractTests: XCTestCase {
    /// This test doesn’t tell us much at all.
    /// It just asserts that there is more than one item in the array of data returned.
    func test_extractDataForAllRows_returnsData() throws {
        let result = try BloombergBillionairesExtract.getAllTheData()
        XCTAssert(result.count > 1)
    }
}
